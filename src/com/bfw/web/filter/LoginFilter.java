package com.bfw.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.bfw.domain.UserInfo;

/**
 * 防止非法登录
 * @author 
 *
 */
@WebFilter(filterName = "LoginFilter", urlPatterns = { "/*" })
public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain fc) throws IOException, ServletException {

		// 把ServletRequest 转化为HttpServletRequest
		HttpServletRequest httprequest = (HttpServletRequest) request;

		// 获取session
		HttpSession seesion = httprequest.getSession();
		UserInfo user = (UserInfo) seesion.getAttribute("userinfo");

		// 获取url
		String url = httprequest.getRequestURI();
		if (url.contains("/assets/")) {
			fc.doFilter(request, response);
		} else if (url.contains("/css/")) {
			fc.doFilter(request, response);
		} else if (url.contains("/font/")) {
			fc.doFilter(request, response);
		}else if (url.contains("/login.jsp")) {
			fc.doFilter(request, response);
		} else if (url.contains("/images/")) {
			fc.doFilter(request, response);
		} else if (url.contains("/js/")) {
			fc.doFilter(request, response);
		} else if (url.contains("/font/")) {
			fc.doFilter(request, response);
		}  else if (url.contains("/images/")) {
			fc.doFilter(request, response);
		}  else if (url.contains("/javascripts/")) {
			fc.doFilter(request, response);
		}  else if (url.contains("/stylesheets/")) {
			fc.doFilter(request, response);
		}  else if (url.contains("/loginServlet")) {
			fc.doFilter(request, response);
		} else if (url.contains("/login.html")) {
			fc.doFilter(request, response);
		} else if (user != null) { // 判断session 里面的用户是否存在
			fc.doFilter(request, response);
		} else {
			httprequest.getRequestDispatcher("/login_info.jsp").forward(httprequest,
					response);

		}

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		//System.out.println("-----初始化过滤器----");
	}

}
