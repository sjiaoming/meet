package com.bfw.mapper;

import java.util.List;

import com.bfw.domain.MeetingroomBespeak;

/**
 * 预约数据库访问接口
 * @author Administrator
 *
 */
public interface MeetingroomBespeakMapper {
	
	/**
	 * 添加预约信息
	 * @param bespeak 预约信息
	 * @return 返回影响的行数
	 */
	public  int addBespeak(MeetingroomBespeak bespeak );
	
	/**
	 * 根据条件查询预约信息
	 * @param bespeak 查询条件
	 * @return 返回多条记录
	 */
	public List<MeetingroomBespeak> getAllMeetingroomBespeak(MeetingroomBespeak bespeak);


	/**
	 * 根据编号查询预约会议室信息
	 * @param bespeakId 预约编号
	 * @return 返回预约的信息
	 */
	public MeetingroomBespeak getMeetingroomBespeak(Integer bespeakId);
	
	
	/**
	 * 修改预约信息
	 * @param bespeak 预约信息
	 * @return
	 */
	public  int updateBespeak(MeetingroomBespeak bespeak);

}
